# Front-end

This project provides a user interface for viewing funding-by-industry-data in a bubble chart.
Clicking on bubbles gives the user detailed information in a table below.

It's written with [Typescript](https://www.typescriptlang.org/) and the major
dependencies it uses are:
* [React](https://github.com/facebook/react)
* [Chart.js](https://github.com/chartjs/Chart.js)

The project is live and running at:
<https://www.assessment.gambitaccepted.com>

The repository can be found at:
<https://bitbucket.org/V-Mann_Nick/assessment-frontend/>

## Setting up the environment

First you're going to need to clone the repository:

```sh
git clone https://V-Mann_Nick@bitbucket.org/V-Mann_Nick/assessment-frontend.git
```

Now install the dependencies:

```sh
yarn
# or
npm install
```

start the development server

```sh
yarn run start
# or
npm run start
```
___
**Done!**
