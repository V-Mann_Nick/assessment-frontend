module.exports = {
  root: true,
  parser: '@typescript-eslint/parser',
  env: {
    browser: true,
    node: true,
    es6: true,
    jasmine: true,
    jest: true,
  },
  parserOptions: {
    sourceType: 'module',
    emcaFeatures: {
      jsx: true,
    },
    emcaVersion: 2020,
  },
  extends: [
    'eslint:recommended',
    'plugin:react/recommended',
    'plugin:@typescript-eslint/recommended',
    'prettier/@typescript-eslint',
    'plugin:prettier/recommended',
    'plugin:import/errors',
    'plugin:import/warnings',
    'plugin:import/typescript',
    'plugin:jsx-a11y/recommended',
  ],
  plugins: ['prettier', 'import', 'react', 'jsx-a11y', '@typescript-eslint'],
  // add your custom rules here
  rules: {
    'no-console': 'off',
    'no-var': 'off',
    eqeqeq: [2, 'smart'],
    'jsx-a11y/no-static-element-interactions': 0,
    'jsx-a11y/click-events-have-key-events': 0,
    'react/prop-types': 1,
  },
}
