import React, { useState, useEffect } from 'react'
import './App.scss'
import Select from '../components/baseComponents/select/Select'
import Visual from '../components/visual/Visual'
import Data from '../components/data/Data'
import {
  FundingData,
  FundingDataAggregated,
  VisualOption,
  Category,
} from '../types'

const selectOptions = [
  {
    value: 'amount',
    text: 'Funding amount',
  },
  {
    value: 'rounds',
    text: 'Number of Fundings',
  },
]

const CATEGORY_COLORS = {
  Health: '#64DD17',
  Games: '#E53935',
  Beauty: '#18FFFF',
  Tools: '#EF6C00',
  Automotive: '#FFFF00',
}

const App: React.FC = () => {
  // fetch data and process it
  const [data, setData] = useState<Array<FundingData>>([])
  const [dataAggregated, setDataAggregated] = useState<
    Array<FundingDataAggregated>
  >([])
  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL as string}/funding/`)
      .then((res) => res.json())
      .then((data: Array<FundingData>) => {
        // this is what is expected as categories
        interface DataAggregate {
          Beauty?: FundingDataAggregated
          Health?: FundingDataAggregated
          Games?: FundingDataAggregated
          Tools?: FundingDataAggregated
          Automotive?: FundingDataAggregated
        }
        const dataAggregatedAsObject: DataAggregate = {}
        data.forEach((dataPoint, idx) => {
          if (!dataAggregatedAsObject[dataPoint.category]) {
            dataAggregatedAsObject[dataPoint.category] = {
              amount: dataPoint.fundingAmount,
              numRounds: 1,
              categoryName: dataPoint.category,
              color: CATEGORY_COLORS[dataPoint.category],
              aggregatedFrom: [idx],
            }
          } else {
            const existingCategory = dataAggregatedAsObject[
              dataPoint.category
            ] as FundingDataAggregated
            existingCategory.numRounds++
            existingCategory.amount += dataPoint.fundingAmount
            existingCategory.aggregatedFrom.push(idx)
          }
        })
        // transoform object to array
        const dataAggregatedAsArray: Array<FundingDataAggregated> = Object.values(
          dataAggregatedAsObject
        )
        setData(data)
        setDataAggregated(dataAggregatedAsArray)
      })
  }, [])

  // handle bubble clicks
  const [tableData, setTableData] = useState<Array<FundingData>>([])
  const [selectedCategory, setSelectedCategory] = useState<Category | null>(
    null
  )
  function handleBubbleClick(clickedBubble: FundingDataAggregated | null) {
    if (clickedBubble === null) {
      setTableData([])
      setSelectedCategory(null)
      return
    }
    const filteredData: Array<FundingData> = []
    clickedBubble.aggregatedFrom.forEach((idx) => filteredData.push(data[idx]))
    setTableData(filteredData)
    setSelectedCategory({
      categoryName: clickedBubble.categoryName,
      color: clickedBubble.color,
    })
  }

  // handle select changes
  const [visualOption, setVisualOption] = useState<VisualOption>('amount')
  function handleSelectChange(e: React.ChangeEvent<HTMLSelectElement>) {
    if (e.target.value !== visualOption) {
      setVisualOption(e.target.value as VisualOption)
      // also hide the table
      setTableData([])
      setSelectedCategory(null)
    }
  }

  return (
    <div className="app">
      <header className="app__header">
        <h1>Funding by Industry Analytics</h1>
        <Select
          options={selectOptions}
          label="Bubble displays"
          handleChange={handleSelectChange}
        />
      </header>
      <hr className="app__hr" />
      <main className="app__main">
        {dataAggregated.length > 0 && (
          <Visual
            data={dataAggregated}
            visualOption={visualOption}
            bubbleClickHandler={handleBubbleClick}
          />
        )}
        {selectedCategory && (
          <Data tableData={tableData} selectedCategory={selectedCategory} />
        )}
      </main>
    </div>
  )
}

export default App
