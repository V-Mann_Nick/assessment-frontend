import React from 'react'
import './Data.scss'
import TableHeading from './tableHeading/TableHeading'
import Table from './table/Table'
import { FundingData, Category } from '../../types'

interface Props {
  tableData: Array<FundingData>
  selectedCategory: Category
}

const Data: React.FC<Props> = ({ tableData, selectedCategory }: Props) => {
  const fundingData = tableData.map(({ fundingAmount }) => fundingAmount)
  const minFunding = Math.min(...fundingData)
  const maxFunding = Math.max(...fundingData)
  return (
    <div className="data">
      <TableHeading
        category={selectedCategory}
        minFunding={minFunding}
        maxFunding={maxFunding}
      />
      <Table data={tableData} />
    </div>
  )
}

export default Data
