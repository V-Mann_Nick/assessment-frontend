import React from 'react'
import { render, cleanup } from '@testing-library/react'
import ReactDOM from 'react-dom'
import Table from '../Table'
import { FundingData } from '../../../../types'

const tableMockData: Array<FundingData> = [
  {
    id: '5ecbdcbfc3f981f4780c05c7',
    category: 'Beauty',
    location: 'United States',
    fundingAmount: 36100,
    announcedDate: 'Aug 6, 2019',
  },
  {
    id: '5ecbdcbfc3f981f4780c05c8',
    category: 'Beauty',
    location: 'United States',
    fundingAmount: 87200,
    announcedDate: 'Jul 26, 2019',
  },
  {
    id: '5ecbdcbfc3f981f4780c05c9',
    category: 'Beauty',
    location: 'United States',
    fundingAmount: 800,
    announcedDate: 'Jan 3, 2018',
  },
]

afterEach(cleanup)

it('renders without crashing', () => {
  const div: HTMLDivElement = document.createElement('div')
  ReactDOM.render(<Table data={tableMockData} />, div)
})

it("'s table head renders correct content", () => {
  const { getByTestId } = render(<Table data={tableMockData} />)

  const thead: HTMLElement = getByTestId('thead')
  expect(thead.childElementCount).toBe(1)

  const tr: HTMLElement = thead.firstElementChild as HTMLElement
  expect(tr.childElementCount).toBe(5)

  const ths: HTMLCollection = tr.children
  const expectedHeader: Array<string> = [
    'id',
    'category',
    'location',
    'funding amount',
    'announcement date',
  ]
  expectedHeader.forEach((header, i) => {
    expect(ths[i]).toHaveTextContent(header)
  })
})

it("'s table body renders correct content'", () => {
  const { getByTestId } = render(<Table data={tableMockData} />)

  const tbody: HTMLElement = getByTestId('tbody')
  expect(tbody.childElementCount).toBe(3)

  const tr: HTMLElement = tbody.firstElementChild as HTMLElement
  expect(tr.childElementCount).toBe(5)

  const tds: HTMLCollection = tr.children
  const expectedRow: Array<string> = [
    '5ecbdcbfc3f981f4780c05c7',
    'Beauty',
    'United States',
    '36,100€',
    'Aug 6, 2019',
  ]
  expectedRow.forEach((val, i) => {
    expect(tds[i]).toHaveTextContent(val)
  })
})

it('matches previous snapshot', () => {
  const { asFragment } = render(<Table data={tableMockData} />)
  expect(asFragment()).toMatchSnapshot()
})
