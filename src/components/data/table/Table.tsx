import React from 'react'
import './Table.scss'
import { FundingData } from '../../../types'
import { formatLargeNumber } from '../../../functions'

interface Props {
  data: Array<FundingData>
}

const Table: React.FC<Props> = ({ data }: Props) => {
  return (
    <div style={{ overflowX: 'auto' }}>
      <table className="table">
        <thead className="table__head" data-testid="thead">
          <tr className="table__row">
            <th className="table__header">id</th>
            <th className="table__header">category</th>
            <th className="table__header">location</th>
            <th className="table__header">funding amount</th>
            <th className="table__header">announcement date</th>
          </tr>
        </thead>
        <tbody className="table__body" data-testid="tbody">
          {data.map(
            (
              { id, category, location, announcedDate, fundingAmount },
              idx: number
            ) => (
              <tr className="table__row" key={idx}>
                <td className="table__data">{id}</td>
                <td className="table__data">{category}</td>
                <td className="table__data">{location}</td>
                <td className="table__data">
                  {formatLargeNumber(fundingAmount)}€
                </td>
                <td className="table__data">{announcedDate}</td>
              </tr>
            )
          )}
        </tbody>
      </table>
    </div>
  )
}

export default Table
