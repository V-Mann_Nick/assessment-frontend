import React from 'react'
import { render, cleanup } from '@testing-library/react'
import ReactDOM from 'react-dom'
import TableHeading from '../TableHeading'

const tableHeading: JSX.Element = (
  <TableHeading
    category={{ color: 'red', categoryName: 'Games' }}
    minFunding={10000}
    maxFunding={50000}
  />
)

afterEach(cleanup)

it('renders without crashing', () => {
  const div: HTMLDivElement = document.createElement('div')
  ReactDOM.render(tableHeading, div)
})

it('renders right content', () => {
  const { getByTestId } = render(tableHeading)
  expect(getByTestId('span')).toHaveTextContent('"Games"')
  expect(getByTestId('h2')).toHaveTextContent(
    /Category.*with funding range between 10,000€ and 50,000€/i
  )
})

it('shows category with colored underline', () => {
  const { getByTestId } = render(tableHeading)
  expect(getByTestId('span')).toHaveStyle('text-decoration: underline')
  expect(getByTestId('span')).toHaveStyle('text-decoration-color: red')
})

it('matches previous snapshot', () => {
  const { asFragment } = render(tableHeading)
  expect(asFragment()).toMatchSnapshot()
})
