import React from 'react'
import './TableHeading.scss'
import { Category } from '../../../types'
import { formatLargeNumber } from '../../../functions'

interface Props {
  category: Category
  minFunding: number
  maxFunding: number
}

const TableHeading: React.FC<Props> = ({
  category,
  minFunding,
  maxFunding,
}: Props) => {
  return (
    <h2 className="table-heading" data-testid="h2">
      Category{' '}
      <span
        style={{
          textDecoration: 'underline',
          textDecorationColor: category.color,
        }}
        data-testid="span"
      >
        &quot;{category.categoryName}&quot;
      </span>{' '}
      with funding range between {formatLargeNumber(minFunding)}€ and{' '}
      {formatLargeNumber(maxFunding)}€
    </h2>
  )
}

export default TableHeading
