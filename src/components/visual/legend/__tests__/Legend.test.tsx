import React from 'react'
import { render, cleanup } from '@testing-library/react'
import ReactDOM from 'react-dom'
import Legend from '../Legend'
import { Category } from '../../../../types/Category'

const legendItems: Array<Category> = [
  {
    categoryName: 'Health',
    color: 'blue',
  },
  {
    categoryName: 'Games',
    color: 'brown',
  },
]

afterEach(cleanup)

it('renders without crashing', () => {
  const div: HTMLDivElement = document.createElement('div')
  ReactDOM.render(<Legend items={legendItems} />, div)
})

it('has the right legend items', () => {
  const { getByText } = render(<Legend items={legendItems} />)
  const healthItem = getByText('Health')
  const gamesItem = getByText('Games')
  expect(healthItem).toBeInTheDocument()
  expect(gamesItem).toBeInTheDocument()
})

it('matches previous snapshot', () => {
  const { asFragment } = render(<Legend items={legendItems} />)
  expect(asFragment()).toMatchSnapshot()
})
