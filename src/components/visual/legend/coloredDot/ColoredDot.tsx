import React from 'react'
import './ColoredDot.scss'

interface Props {
  diameter: string | number
  color: string
}

const ColoredDot: React.FC<Props> = ({ diameter, color }: Props) => {
  diameter = typeof diameter === 'string' ? diameter : `${diameter}px`
  return (
    <div
      className="colored-dot"
      style={{ width: diameter, height: diameter, background: color }}
      data-testid="div"
    />
  )
}

export default ColoredDot
