import React from 'react'
import { render, cleanup } from '@testing-library/react'
import ReactDOM from 'react-dom'
import ColoredDot from '../ColoredDot'

afterEach(cleanup)

it('renders without crashing', () => {
  const div: HTMLDivElement = document.createElement('div')
  ReactDOM.render(<ColoredDot diameter="1rem" color="red" />, div)
})

it('matches previous snapshot', () => {
  const { asFragment } = render(<ColoredDot diameter="1rem" color="red" />)
  expect(asFragment()).toMatchSnapshot()
})

it('has the right style', () => {
  const { getByTestId } = render(<ColoredDot diameter="1rem" color="red" />)
  const dot = getByTestId('div')
  expect(dot).toHaveStyle('width: 1rem')
  expect(dot).toHaveStyle('height: 1rem')
  expect(dot).toHaveStyle('background: red')
})
