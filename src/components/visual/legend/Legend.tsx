import React from 'react'
import './Legend.scss'
import { Category } from '../../../types'
import ColoredDot from './coloredDot/ColoredDot'

interface Props {
  items: Array<Category>
}

const dotDiameter = '1rem'

const Legend: React.FC<Props> = ({ items }: Props) => {
  return (
    <ul className="legend">
      {items.map(({ color, categoryName }: Category, idx: number) => (
        <li className="legend__item" key={idx}>
          <ColoredDot color={color} diameter={dotDiameter} />
          {categoryName}
        </li>
      ))}
    </ul>
  )
}

export default Legend
