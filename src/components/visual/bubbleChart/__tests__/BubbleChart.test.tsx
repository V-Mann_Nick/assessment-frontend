import React from 'react'
import { render, cleanup } from '@testing-library/react'
import ReactDOM from 'react-dom'
import BubbleChart from '../BubbleChart'
import 'jest-canvas-mock'
import { FundingDataAggregated } from '../../../../types'

const mockData: Array<FundingDataAggregated> = [
  {
    categoryName: 'Health',
    amount: 500000,
    numRounds: 1,
    color: 'blue',
    aggregatedFrom: [],
  },
  {
    categoryName: 'Games',
    amount: 1000000,
    numRounds: 6,
    color: 'red',
    aggregatedFrom: [],
  },
  {
    categoryName: 'Beauty',
    amount: 150000,
    numRounds: 4,
    color: 'black',
    aggregatedFrom: [],
  },
  {
    categoryName: 'Tools',
    amount: 451567,
    numRounds: 7,
    color: 'green',
    aggregatedFrom: [],
  },
  {
    categoryName: 'Automotive',
    amount: 885135,
    numRounds: 3,
    color: 'brown',
    aggregatedFrom: [],
  },
]

const clickHandler = () => null

const BubbleChartAmount = (
  <BubbleChart
    data={mockData}
    visualOption="amount"
    bubbleClickHandler={clickHandler}
  />
)

const BubbleChartRounds = (
  <BubbleChart
    data={mockData}
    visualOption="rounds"
    bubbleClickHandler={clickHandler}
  />
)

afterEach(cleanup)

it('renders without crashing', () => {
  const div: HTMLDivElement = document.createElement('div')
  ReactDOM.render(BubbleChartAmount, div)
  ReactDOM.render(BubbleChartRounds, div)
})

it('with amount-option matches previous snapshot', async () => {
  const { getByTestId } = render(BubbleChartAmount)
  const canvas: HTMLElement = getByTestId('canvas')
  expect(
    (canvas as HTMLCanvasElement).getContext('2d')?.__getEvents()
  ).toMatchSnapshot()
})

it('with rounds-option matches previous snapshot', async () => {
  const { getByTestId } = render(BubbleChartRounds)
  const canvas: HTMLElement = getByTestId('canvas')
  expect(
    (canvas as HTMLCanvasElement).getContext('2d')?.__getEvents()
  ).toMatchSnapshot()
})
