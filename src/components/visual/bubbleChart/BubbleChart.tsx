import React, { useRef, useEffect } from 'react'
import Chart from 'chart.js'
import ChartDataLabels from 'chartjs-plugin-datalabels'
import {
  FundingDataAggregated,
  VisualOption,
  CategoryName,
} from '../../../types'
import { formatLargeNumber } from '../../../functions'

interface Props {
  visualOption: VisualOption
  data: Array<FundingDataAggregated>
  bubbleClickHandler: (clickedBubble: FundingDataAggregated | null) => void
  maxBubbleRadius?: number
}

const MAX_CANVAS_WIDTH = 800

const BubbleChart: React.FC<Props> = ({
  visualOption,
  data,
  bubbleClickHandler: clickHandler,
  maxBubbleRadius = 70,
}: Props) => {
  const canvasRef = useRef<HTMLCanvasElement>(null)

  /* represents a data point for chartjs */
  interface Point {
    x: CategoryName
    y: number
    r: number
  }

  /*
     This code is declared as function for double use on initial render
     and with onResize-callback from chartjs for scaling maxBubbleRadius
  */
  function computeDataForChart(scaledMaxBubbleRadius: number): Array<Point> {
    let dataForChart: Array<Point>
    if (visualOption === 'amount') {
      const maxAmount: number = Math.max(
        ...data.map((dataPoint) => dataPoint.amount)
      )
      dataForChart = data.map(({ categoryName, numRounds, amount }) => {
        const scaledBubbleRadius =
          Math.sqrt(amount / maxAmount) * scaledMaxBubbleRadius
        return {
          x: categoryName,
          y: numRounds,
          r: scaledBubbleRadius,
        }
      })
    } else {
      const maxNumRounds: number = Math.max(
        ...data.map((dataPoint) => dataPoint.numRounds)
      )
      dataForChart = data.map(
        ({ categoryName: category, numRounds, amount }) => {
          const scaledBubbleRadius =
            Math.sqrt(numRounds / maxNumRounds) * scaledMaxBubbleRadius
          return {
            x: category,
            y: amount,
            r: scaledBubbleRadius,
          }
        }
      )
    }
    return dataForChart
  }

  /*
    scales maximum bubble radius according to canvas width
  */
  function scaleMaxBubbleRadius(canvasWidth: number): number {
    if (canvasWidth < MAX_CANVAS_WIDTH)
      return (canvasWidth / MAX_CANVAS_WIDTH) * maxBubbleRadius
    else return maxBubbleRadius
  }

  useEffect(() => {
    const bubbleChart = new Chart(canvasRef.current as HTMLCanvasElement, {
      plugins: [ChartDataLabels],
      type: 'bubble',
    })

    // scale maxBubbleRadius
    const scaledMaxBubbleRadius = scaleMaxBubbleRadius(
      bubbleChart.width as number
    )
    // use categories as lables
    const labels: Array<CategoryName> = data.map(
      (dataPoint) => dataPoint.categoryName
    )
    // create array of colors
    const colors: Array<string> = data.map((dataPoint) => dataPoint.color)
    // add data to bubbleChart
    const dataForChart = computeDataForChart(scaledMaxBubbleRadius)
    bubbleChart.data = {
      labels,
      datasets: [
        {
          datalabels: {
            color: 'white',
            backgroundColor: 'black',
            borderRadius: 10,
            padding: {
              top: 6,
            },
          },
          data: dataForChart,
          backgroundColor: colors,
        },
      ],
    }

    // determine yMax and yMin making a conversion from bubble radius
    // with pixel-unit to y-units
    const chartHeight = bubbleChart.chartArea.bottom - bubbleChart.chartArea.top
    const yMaxVal = Math.max(...dataForChart.map(({ y }) => y))
    const yUnitsPerPixel = yMaxVal / chartHeight
    const maxOutOfBoundsPointTop = dataForChart.reduce(
      (maxOutOfBoundsPoint, point) =>
        point.y + point.r * yUnitsPerPixel - yMaxVal >
        maxOutOfBoundsPoint.y + maxOutOfBoundsPoint.r * yUnitsPerPixel - yMaxVal
          ? point
          : maxOutOfBoundsPoint,
      dataForChart[0]
    )
    const maxOutOfBoundsPointBottom = dataForChart.reduce(
      (maxOutOfBoundsPoint, point) =>
        point.y - point.r * yUnitsPerPixel <
        maxOutOfBoundsPoint.y - maxOutOfBoundsPoint.r * yUnitsPerPixel
          ? point
          : maxOutOfBoundsPoint,
      dataForChart[0]
    )
    const EXTRA_PADDING_SCALER = 1.2
    const topPadding =
      maxOutOfBoundsPointTop.r * yUnitsPerPixel * EXTRA_PADDING_SCALER
    // because maxOutOfBoundsPointBottom can be well over 0 this number
    // is relative to maxOutOfBoundsPointBottom.y
    const bottomPaddingFromPoint =
      maxOutOfBoundsPointBottom.r * yUnitsPerPixel * EXTRA_PADDING_SCALER
    let yMax = maxOutOfBoundsPointTop.y + topPadding
    let yMin = maxOutOfBoundsPointBottom.y - bottomPaddingFromPoint
    if (visualOption === 'rounds') {
      // set yMax/yMin to next biggest/lowest 100000-unit-tick
      yMax = Math.floor(yMax / 100000 + 1) * 100000
      yMin = Math.min(Math.floor(yMin / 100000) * 100000, 0)
    } else {
      // set yMax/yMin to next biggest/lowest 1-unit-tick
      yMax = Math.floor(yMax + 1)
      yMin = Math.min(Math.floor(yMin), 0)
    }

    bubbleChart.options = {
      hover: {
        animationDuration: 0,
      },
      onHover: (event: MouseEvent, activeElements) => {
        if (canvasRef.current !== null)
          canvasRef.current.style.cursor =
            activeElements.length > 0 ? 'pointer' : 'default'
      },
      onResize: (newSize: Chart.ChartSize) => {
        if (bubbleChart.data.datasets)
          bubbleChart.data.datasets[0].data = computeDataForChart(
            scaleMaxBubbleRadius(newSize.width)
          )
        bubbleChart.update()
      },
      onClick: (event, active: Array<any>) => {
        if (active.length > 0) clickHandler(data[active[0]._index])
        else clickHandler(null)
      },
      plugins: {
        datalabels: {
          formatter: (val, { dataIndex }) =>
            visualOption === 'amount'
              ? `${formatLargeNumber(data[dataIndex].amount)}€`
              : data[dataIndex].numRounds,
          anchor: 'start',
          align: 'start',
          offset: 0,
        },
      },
      tooltips: {
        position: 'nearest',
        callbacks: {
          label: (ctx) => {
            return String(ctx.label)
          },
          footer: (ctx) =>
            visualOption === 'amount'
              ? `number of fundings: ${ctx[0].value}`
              : `funding total: ${formatLargeNumber(ctx[0].value as string)}€`,
        },
      },
      legend: {
        display: false,
      },
      scales: {
        xAxes: [
          {
            scaleLabel: {
              display: true,
              labelString: 'category',
              fontColor: 'black',
            },
            offset: true,
            type: 'category',
            gridLines: {
              display: false,
              color: 'black',
            },
            ticks: {
              display: false,
            },
          },
        ],
        yAxes: [
          {
            // offset: true,
            scaleLabel: {
              display: true,
              labelString:
                visualOption === 'amount'
                  ? 'number of fundings'
                  : 'funding total',
              fontColor: 'black',
            },
            ticks: {
              max: yMax,
              min: yMin,
              beginAtZero: true,
              fontColor: 'black',
              stepSize: visualOption === 'amount' ? 1 : 100000,
              callback: (value) => {
                if (String(value).includes('-')) return ''
                return visualOption === 'rounds'
                  ? `${formatLargeNumber(Number(value))}€`
                  : value
              },
            },
          },
        ],
      },
    }
    bubbleChart.update()

    return function cleanup() {
      bubbleChart.destroy()
    }
  })

  return (
    <canvas ref={canvasRef} className="bubble-chart" data-testid="canvas" />
  )
}

export default BubbleChart
