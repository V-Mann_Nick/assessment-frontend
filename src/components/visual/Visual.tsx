import React from 'react'
import './Visual.scss'
import Legend from './legend/Legend'
import BubbleChart from './bubbleChart/BubbleChart'
import { Category, FundingDataAggregated, VisualOption } from '../../types'

interface Props {
  data: Array<FundingDataAggregated>
  visualOption: VisualOption
  bubbleClickHandler: (clickedBubble: FundingDataAggregated | null) => void
}

const Visual: React.FC<Props> = ({
  data,
  visualOption,
  bubbleClickHandler,
}: Props) => {
  const legend: Array<Category> = data.map(({ color, categoryName }) => ({
    color,
    categoryName,
  }))
  return (
    <div className="visual">
      <Legend items={legend} />
      <BubbleChart
        data={data}
        visualOption={visualOption}
        bubbleClickHandler={bubbleClickHandler}
      />
    </div>
  )
}

// When Bubble gets clicked the app state updates and causes rerender.
// This allows rerender only if relevant props are changed.
export default React.memo(Visual, (prevProps, nextProps) => {
  const visualOptionDiffers = prevProps.visualOption !== nextProps.visualOption
  const dataDiffers = prevProps.data !== nextProps.data
  return !visualOptionDiffers && !dataDiffers
})
