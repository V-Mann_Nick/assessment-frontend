import React from 'react'
import './Select.scss'

interface Option {
  value: string | number
  text: string | number
}

interface Props {
  options: Array<Option>
  fontSize?: number | string
  handleChange?: (event: React.ChangeEvent<HTMLSelectElement>) => void
  label?: string
}

const Select: React.FC<Props> = ({
  options,
  fontSize = '1rem',
  handleChange,
  label,
}: Props) => {
  fontSize = typeof fontSize === 'string' ? fontSize : `${fontSize}px`
  return (
    <div className="select">
      {Boolean(label) && (
        <label
          htmlFor={label}
          className="select__label"
          style={{ fontSize }}
          data-testid="label"
        >
          {label}
        </label>
      )}
      <div className="select__container">
        <select
          id={label ? label : ''}
          onChange={handleChange}
          onBlur={handleChange}
          style={{ fontSize }}
          data-testid="select"
        >
          {options.map(({ value, text }: Option, idx: number) => (
            <option value={value} key={idx}>
              {text}
            </option>
          ))}
        </select>
      </div>
    </div>
  )
}

export default Select
