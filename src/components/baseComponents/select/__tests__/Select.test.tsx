import React from 'react'
import { render, cleanup } from '@testing-library/react'
import ReactDOM from 'react-dom'
import Select from '../Select'

const selectOptions = [
  {
    value: 'amount',
    text: 'Funding amount',
  },
  {
    value: 'rounds',
    text: 'Number of Fundings',
  },
]

afterEach(cleanup)

it('renders without crashing', () => {
  const div: HTMLDivElement = document.createElement('div')
  ReactDOM.render(<Select options={selectOptions} />, div)
})

it('renders label', () => {
  const { getByText } = render(
    <Select options={selectOptions} label="test label" />
  )
  expect(getByText('test label')).toBeInTheDocument()
})

it('renders options', () => {
  const { getByText } = render(
    <Select options={selectOptions} label="test label" />
  )
  const fundingOption: HTMLElement = getByText('Number of Fundings')
  expect(fundingOption).toBeInTheDocument()
  expect(fundingOption).toHaveValue('rounds')

  const amountOption: HTMLElement = getByText('Funding amount')
  expect(amountOption).toBeInTheDocument()
  expect(amountOption).toHaveValue('amount')
})

it('render fontSize prop correctly', () => {
  const { getByTestId } = render(
    <Select options={selectOptions} label="test label" fontSize={10} />
  )
  expect(getByTestId('select')).toHaveStyle('font-size: 10px')
  expect(getByTestId('label')).toHaveStyle('font-size: 10px')
})

it('matches previous snapshot', () => {
  const { asFragment } = render(
    <Select options={selectOptions} label="test label" />
  )
  expect(asFragment()).toMatchSnapshot()
})
