/*
   Formats a large number to a human readable string
   example: 1000000 -> 1.000.000
*/
export function formatLargeNumber(largeNumber: number | string): string {
  return String(largeNumber)
    .split('')
    .reverse()
    .map((char, idx) => {
      if (idx !== 0 && idx % 3 === 0) return `${char},`
      else return char
    })
    .reverse()
    .join('')
}
