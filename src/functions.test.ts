import { formatLargeNumber } from './functions'

it('formats large numbers correctly', () => {
  expect(formatLargeNumber(1)).toBe('1')
  expect(formatLargeNumber(111)).toBe('111')
  expect(formatLargeNumber(1111)).toBe('1,111')
  expect(formatLargeNumber(1111111)).toBe('1,111,111')
})
