/*
  all possible values for a category
*/
export type CategoryName =
  | 'Beauty'
  | 'Health'
  | 'Games'
  | 'Tools'
  | 'Automotive'

/*
  all possible values for location
*/
export type Location =
  | 'United States'
  | 'Indonesia'
  | 'Brazil'
  | 'Ukraine'
  | 'Kosovo'

/*
  Options, which can be passed to bubble chart:
  - amount: bubbles will represent total amount of
    fuding per category
  - rounds: bubbles will represent number of fundings
    per category
*/
export type VisualOption = 'amount' | 'rounds'

/*
  represents a category. Used for legend
  and extended by fundingDataAggregated
*/
export type Category = {
  color: string
  categoryName: CategoryName
}

/*
  data type, which is passed to bubble chart
*/
export type FundingDataAggregated = Category & {
  amount: number
  numRounds: number
  aggregatedFrom: Array<number>
}

/*
  data type, which represent data coming from api
*/
export type FundingData = {
  id: string
  category: CategoryName
  location: Location
  fundingAmount: number
  announcedDate: string
}
